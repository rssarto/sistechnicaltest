package com.sis.test.viewmodel;

import java.io.Serializable;
import java.util.Date;

public class TeamView implements Serializable {
	
	private static final long serialVersionUID = 1916306904732305535L;
	
	private int id;
	private String name;
	private String city;
	private String owner;
	private int stadiumCapacity;
	private String competition;
	private int numberPlayers;
	private Date dateCreation;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public int getStadiumCapacity() {
		return stadiumCapacity;
	}
	public void setStadiumCapacity(int stadiumCapacity) {
		this.stadiumCapacity = stadiumCapacity;
	}
	public String getCompetition() {
		return competition;
	}
	public void setCompetition(String competition) {
		this.competition = competition;
	}
	public int getNumberPlayers() {
		return numberPlayers;
	}
	public void setNumberPlayers(int numberPlayers) {
		this.numberPlayers = numberPlayers;
	}
	
	@Override
	public String toString() {
		return "TeamView [id=" + id + ", name=" + name + ", city=" + city + ", owner=" + owner + ", stadiumCapacity="
				+ stadiumCapacity + ", competition=" + competition + ", numberPlayers=" + numberPlayers
				+ ", dateCreation=" + dateCreation + "]";
	}
	public TeamView(String name, String city, String owner, int stadiumCapacity, String competition,
			int numberPlayers, Date dateCreation) {
		super();
		this.name = name;
		this.city = city;
		this.owner = owner;
		this.stadiumCapacity = stadiumCapacity;
		this.competition = competition;
		this.numberPlayers = numberPlayers;
		this.dateCreation = dateCreation;
	}
	
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public TeamView(){}
}
