package com.sis.test.mapper.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.stereotype.Component;

@Component
public class ListMapper extends DozerBeanMapper {
    /**
     * Maps each element of the list to the provided destinaTion class object instance
     * 
     * @param source
     *            - source list, containing objects to be mapped
     * @param destinationClass
     *            type of the destination object
     * @return list of destination class instances
     * @throws MappingException
     */
    public <T> List<T> map(Collection<?> source, Class<T> destinationClass) throws MappingException {
        List<T> returnList = new ArrayList<T>();
        for (Object object : source) {
            returnList.add(super.map(object, destinationClass));
        }
        return returnList;
    }
}
