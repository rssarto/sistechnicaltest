package com.sis.test;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sis.test.mapper.util.ListMapper;
import com.sis.test.model.TeamModel;
import com.sis.test.service.TeamService;
import com.sis.test.viewmodel.TeamView;

@RestController
public class TeamController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private ListMapper mapper;

	@RequestMapping(value="rest/list", method=RequestMethod.GET)
	public ResponseEntity<List<TeamView>> list(){
		logger.info("list() method begins");
		List<TeamView> teamViews = null;
		List<TeamModel> teamModels = teamService.listAllTeams();
		if( teamModels != null ){
			teamViews = mapper.map(teamModels, TeamView.class);
			logger.info("list() method result: " + teamViews);
			return new ResponseEntity<List<TeamView>>(teamViews, HttpStatus.OK);
		}else{
			logger.info("list() method no teams found.");
		}
		return new ResponseEntity<List<TeamView>>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value="rest/listByStadiumCapacity", method=RequestMethod.GET)
	public ResponseEntity<List<TeamView>> listByStadiumCapacity(){
		logger.info("listByStadiumCapacity() method begins");
		List<TeamView> teamViews = null;
		List<TeamModel> teamModels = teamService.listAllTeamsByStadiumCapacity();
		if( teamModels != null ){
			teamViews = mapper.map(teamModels, TeamView.class);
			logger.info("listByStadiumCapacity() method result: " + teamViews);
			return new ResponseEntity<List<TeamView>>(teamViews, HttpStatus.OK);
		}
		return new ResponseEntity<List<TeamView>>(HttpStatus.NOT_FOUND);		
	}
	
	@RequestMapping(value="rest/byId", method=RequestMethod.GET)
	public ResponseEntity<TeamView> byId(@RequestParam(value="id") int id){
		logger.info("byId() method begins - id: " + id);
		TeamModel teamModel = teamService.byId(id);
		if( teamModel != null ){
			TeamView teamView = mapper.map(teamModel, TeamView.class);
			logger.info("byId() method result: " + teamView.toString());
			return new ResponseEntity<TeamView>(teamView, HttpStatus.OK);
		}
		return new ResponseEntity<TeamView>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value="rest/addTeam", method=RequestMethod.PUT)
	public ResponseEntity<List<TeamView>> addTeam(@RequestBody TeamView team){
		logger.info("addTeam() method begins - new team: " + team.toString());
		TeamModel teamModel = mapper.map(team, TeamModel.class);
		teamModel = teamService.add(teamModel);
		List<TeamModel> teamModels = teamService.listAllTeams();
		List<TeamView> teamViews = null;
		if( teamModels != null && teamModels.contains(teamModel) ){
			teamViews = mapper.map(teamModels, TeamView.class);
			logger.info("addTeam() method result: " + teamViews.size());
			return new ResponseEntity<List<TeamView>>(teamViews, HttpStatus.OK);
		}else{
			logger.info("addTeam() method - new team not recorded.");
		}
		return new ResponseEntity<List<TeamView>>(teamViews, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
