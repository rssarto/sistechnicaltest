package com.sis.test.serviceimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.sis.test.model.TeamModel;
import com.sis.test.service.TeamService;

@Component
public class TeamServiceImpl implements TeamService {
	
	private static Map<Integer, TeamModel> teamList = new LinkedHashMap<Integer, TeamModel>();
	private static int teamIndex;

	@Override
	public List<TeamModel> listAllTeams() {
		if( teamList.size() > 0 ){
			return new ArrayList<TeamModel>(teamList.values());
		}
		return null;
	}

	@Override
	public List<TeamModel> listAllTeamsByStadiumCapacity() {
		if( teamList.size() > 0 ){
			List<TeamModel> teamModels = new ArrayList<TeamModel>(teamList.values());
			Collections.sort(teamModels);
			return teamModels;
		}
		return null;
	}

	@Override
	public TeamModel byId(int id) {
		return teamList.get(new Integer(id));
	}

	@Override
	public TeamModel add(TeamModel model) {
		model.setId(++teamIndex);
		teamList.put(new Integer(teamIndex), model);
		return model;
	}
	
}
