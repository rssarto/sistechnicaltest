package com.sis.test.model;

import java.io.Serializable;
import java.util.Date;

public class TeamModel implements Serializable, Comparable<TeamModel> {

	private static final long serialVersionUID = 7684071016382044980L;
	
	private int id;
	private String name;
	private String city;
	private String owner;
	private int stadiumCapacity;
	private String competition;
	private int numberPlayers;
	private Date dateCreation;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public int getStadiumCapacity() {
		return stadiumCapacity;
	}
	public void setStadiumCapacity(int stadiumCapacity) {
		this.stadiumCapacity = stadiumCapacity;
	}
	public String getCompetition() {
		return competition;
	}
	public void setCompetition(String competition) {
		this.competition = competition;
	}
	public int getNumberPlayers() {
		return numberPlayers;
	}
	public void setNumberPlayers(int numberPlayers) {
		this.numberPlayers = numberPlayers;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	@Override
	public String toString() {
		return "TeamModel [name=" + name + ", city=" + city + ", owner=" + owner + ", stadiumCapacity="
				+ stadiumCapacity + ", competition=" + competition + ", numberPlayers=" + numberPlayers
				+ ", dateCreation=" + dateCreation + "]";
	}
	public TeamModel(String name, String city, String owner, int stadiumCapacity, String competition, int numberPlayers,
			Date dateCreation) {
		this.name = name;
		this.city = city;
		this.owner = owner;
		this.stadiumCapacity = stadiumCapacity;
		this.competition = competition;
		this.numberPlayers = numberPlayers;
		this.dateCreation = dateCreation;
	}
	
	public TeamModel(){}
	
	@Override
	public int compareTo(TeamModel o) {
		//return new Integer(this.stadiumCapacity).compareTo(new Integer(o.getStadiumCapacity()));
		return new Integer(o.stadiumCapacity).compareTo(new Integer(this.stadiumCapacity));
	}
	@Override
	public boolean equals(Object other) {
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof TeamModel))return false;
	    TeamModel otherTeam = (TeamModel) other;
	    if( otherTeam.getId() == this.getId() ) return true;
	    return false;
	}
	
	
}
