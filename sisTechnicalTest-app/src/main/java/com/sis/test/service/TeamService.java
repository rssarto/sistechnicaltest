package com.sis.test.service;

import java.util.List;

import com.sis.test.model.TeamModel;

public interface TeamService {
	
	abstract List<TeamModel> listAllTeams();
	abstract List<TeamModel> listAllTeamsByStadiumCapacity();
	abstract TeamModel byId(int id);
	abstract TeamModel add(TeamModel model);

}
