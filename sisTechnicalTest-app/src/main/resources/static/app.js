var appSisTechnicalTest = angular.module('AppSisTechnicalTest',['ui.router']);
appSisTechnicalTest.config(function($stateProvider){
    $stateProvider
        .state({
            name:'login',
            url:'/login',
            templateUrl:'./templates/login.html',
            controller:'navigation as controller',
        })
        .state({
        	name:'new-team',
        	url:'/new',
        	templateUrl:'./templates/new-team-part.html',
        	controller:'NewTeamController'
        })
        .state({
            name:'list-all',
            url:'/listaAll/:listOpt',
            templateUrl:'./templates/all-teams.html',
            controller:'ListTeamsController'
        });
        
});

var newTeamController = appSisTechnicalTest.controller("NewTeamController",['$scope','$http','$timeout', function($scope,$http,$timeout){
    $scope.newTeam = null;
    $scope.result = "";

    $scope.add = function(){
        $http.put('./rest/addTeam', $scope.newTeam).then(
            function(result){
                $scope.result = "success";
                $scope.newTeam = null;
            },
            function(result){
                $scope.result = "fail";
                console.error(data);
            }            
        );
        $timeout(
            function(){
                $scope.result = "";
            }, 
            5000);
    }
}]);

var listTeams = appSisTechnicalTest.controller("ListTeamsController",['$scope', '$http', '$stateParams', function($scope, $http, $stateParams){
    $scope.teamList = null;
    $scope.teamDetail;
    $scope.listOpt = $stateParams.listOpt;

    console.info("$stateParams.listOpt: " + $stateParams.listOpt);
    if( $stateParams.listOpt === '1'){
        $http.get('./rest/list')
            .success(function(result){
                $scope.teamList = result;
            })
            .error(function(data, status){
                console.error(data);
            });
    }else{
        $http.get('./rest/listByStadiumCapacity').then(
            function(result){
                console.debug(result);
                $scope.teamList = result;
            },
            function(result){
                console.error(result);
            }
        );
    }

    $scope.byId = function(id){
        $http.get('./rest/byId?id=' + id).then(
            function(result){
                $scope.teamDetail = result;
            },
            function(data, status){
                console.error(data);
            }            
        );
    }

}]);

var navigation = appSisTechnicalTest.controller("navigation",['$rootScope', '$http', '$location', function($rootScope, $http, $location){
  var self = this

  var authenticate = function(credentials, callback) {

    var headers = credentials ? {authorization : "Basic "
        + btoa(credentials.username + ":" + credentials.password)
    } : {};

    $http.get('user', {headers : headers}).then(function(response) {
      if (response.data.name) {
        $rootScope.authenticated = true;
      } else {
        $rootScope.authenticated = false;
      }
      callback && callback();
    }, function() {
      $rootScope.authenticated = false;
      callback && callback();
    });

  }

  authenticate();
  self.credentials = {};
  self.login = function() {
      authenticate(self.credentials, function() {
        if ($rootScope.authenticated) {
          $location.path("/");
          self.error = false;
        } else {
          $location.path("/login");
          self.error = true;
        }
      });
  };
  
  self.logout = function() {
	  $http.post('logout', {}).finally(function() {
	    $rootScope.authenticated = false;
	    $location.path("/");
	  });
  }  
}]);
