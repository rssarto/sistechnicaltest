package com.sis.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;
import com.sis.test.model.TeamModel;
import com.sis.test.service.TeamService;
import com.sis.test.viewmodel.TeamView;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamController.class)
public class TeamControllerTests {
	
	@Autowired
	private MockMvc mvc;

	@Autowired
	private TeamService teamService;
	
	@Before
	public void prepareTeams(){
		teamService.add(new TeamModel("Santos", "Santos", "Pelé", 15000, "Paulistão", 25, new Date()));
		teamService.add(new TeamModel("Portugues", "São Paulo", "Paulo de Souza", 20000, "Paulistão", 22, new Date()));
	}
	
	@Test
	public void addTeams() throws Exception{
		TeamView teamView = new TeamView("Corinthians", "São Paulo", "Andres", 45000, "Paulistão", 25, new Date());
		Gson gson = new Gson();
		String json = gson.toJson(teamView); 
		this.mvc.perform(put("/rest/addTeam")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
				.andExpect(status().isOk());
		
		teamView = new TeamView("Palmeiras", "São Paulo", "Paulo", 12000, "Paulistão", 30, new Date());
		json = gson.toJson(teamView); 
		this.mvc.perform(put("/rest/addTeam")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
				.andExpect(status().isOk());

	}
	
	@Test
	public void getExistingTeamById() throws Exception{
		this.mvc.perform(get("/rest/byId?id=1")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void getNotExistingTeamById() throws Exception{
		this.mvc.perform(get("/rest/byId?id=1000")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void getAllTeamsList() throws Exception{
		this.mvc.perform(get("/rest/list")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void getTeamsByStadiumCapacity() throws Exception{
		this.mvc.perform(get("/rest/listByStadiumCapacity")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
}
