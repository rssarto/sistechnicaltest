#=================================================================
# TECHNOLOGIES INVOLVED
#=================================================================
This is built upon AngularJS, Bootstrap and Spring Boot. These two frameworks are good opportunities 
to learn fast and develop really good apps wich can offer fantastic user experience.

#=================================================================
# HOW TO START DE APPLICATION
#=================================================================
You can just run the application as Spring Boot App.

To access the application use the URL below:
	http://localhost:8080/sistechnicaltest/
	
Then in the browser you can see the navigation bar with the option below:
	New team - Used to create a new team;
	All teams - Lists all teams;
	Stadium Capacity - Lists all teams by stadium capacity in descending order.